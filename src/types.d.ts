export type Color = 'Marron y Blanco' | 'Marron y negro'
export type Breed = 'Brahman' | 'Brangus' | 'Bradford'
export enum Sex {
  MALE = 'Macho',
  FEMALE = 'Hembra'
}

export interface DBConnection {
  host: string
  user: string
  password: string
  port: number | string
  database: string
}

export interface Animales {
  id: number
  color: Color
  breed: Breed
  idcabanacriadora: number
  sex: Sex
}

// no se para que
