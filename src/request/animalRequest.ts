import { check } from 'express-validator'

export const createAnimalRequest = [
  check('name')
    .notEmpty().withMessage('This param is required.')
    .isLength({ min: 4 }).withMessage('This parameter must have at least 4 characters')
    .isString().withMessage('This param is a text')
]
