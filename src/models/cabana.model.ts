import { Table, Column, DataType, Model } from 'sequelize-typescript'

@Table({ tableName: 'cabana', underscored: true, updatedAt: false, createdAt: false })
export default class Cabana extends Model {
  @Column({ type: DataType.INTEGER, primaryKey: true, autoIncrement: true })
    id!: number

  @Column({ type: DataType.STRING, allowNull: false })
    name!: string
}
