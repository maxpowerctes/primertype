import { Table, Column, DataType, Model, BelongsTo, ForeignKey } from 'sequelize-typescript'
import Cabana from './cabana.model'

@Table({ tableName: 'animal', underscored: true, updatedAt: false, createdAt: false })
export default class Animal extends Model {
  @Column({ type: DataType.INTEGER, primaryKey: true, autoIncrement: true })
    id!: number

  @Column({ type: DataType.STRING, allowNull: false })
    name!: string

  @ForeignKey(() => Cabana)
  @Column({ type: DataType.INTEGER, allowNull: true })
    cabanacriadora!: number

  @BelongsTo(() => Cabana)
    cabana!: Cabana

  @Column({ type: DataType.STRING, allowNull: true })
    color!: string
}
