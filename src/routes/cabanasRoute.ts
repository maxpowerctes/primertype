import { Router } from 'express'
import { getCabana, getCabanaById, createCabana, deleteCabana, updateCabana } from '../controllers/cabanaController'
import { createCabanaRequest } from '../request/cabanaRequest'
import { validator } from '../middlewares/validatorRequestMiddleware'

const router = Router()

router.get('/', getCabana)
router.post('/', validator(createCabanaRequest), createCabana)
router.get('/:id', getCabanaById)
router.put('/:id', updateCabana)
router.delete('/:id', deleteCabana)

export default router
