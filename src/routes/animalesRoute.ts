import { Router } from 'express'
import { getAnimales, getAnimalesById, createAnimales, deleteAnimales, updateAnimales } from '../controllers/animalController'
import { createAnimalRequest } from '../request/animalRequest'
import { validator } from '../middlewares/validatorRequestMiddleware'

const router = Router()

router.get('/', getAnimales)
router.post('/', validator(createAnimalRequest), createAnimales)
router.get('/:id', getAnimalesById)
router.put('/:id', updateAnimales)
router.delete('/:id', deleteAnimales)

export default router
