import express from 'express'
import { sendDataMiddleware } from '../middlewares/sendData'
import animalRouter from './animalesRoute'
import cabanaRouter from './cabanasRoute'

const router = express()

console.log(process.env.PORT)

router.use(sendDataMiddleware)
router.use('/cabana', cabanaRouter)
router.use('/animal', animalRouter)

export default router
