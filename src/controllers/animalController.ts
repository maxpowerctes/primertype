/* eslint-disable @typescript-eslint/no-misused-promises */
import { NextFunction, Request, RequestHandler, Response } from 'express'
import Animal from '../models/animal.model'

export const getAnimales: RequestHandler = async (_req: Request, res: Response, next: NextFunction) => {
  try {
    const animal: Animal[] = await Animal.findAll({ attributes: ['id', 'name', 'cabanacriadora', 'color'] })
    return res.sendData(animal, animal.length)
  } catch (e) {
    return next(e)
  }
}

export const getAnimalesById: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const animal = await Animal.findByPk(id)
    return res.sendData(animal, 1)
  } catch (e) {
    return next(e)
  }
}

export const createAnimales: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { name, cabanacriadora, color } = req.body
    const animal = await Animal.create({ name, cabanacriadora, color })
    return res.sendData([animal], 1)
  } catch (e) {
    return next(e)
  }
}

export const updateAnimales: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = parseInt(req.params.id)
    const { name, cabanacriadora, color } = req.body
    const animalUpdated = await Animal.update({ name, cabanacriadora, color }, { where: { id }, returning: true })
    return res.sendData(animalUpdated, 1)
  } catch (e) {
    return next(e)
  }
}

export const deleteAnimales: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const animal = await Animal.destroy({ where: { id } })
    return res.sendData(animal, 1)
  } catch (e) {
    return next(e)
  }
}
