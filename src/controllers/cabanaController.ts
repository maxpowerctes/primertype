/* eslint-disable @typescript-eslint/no-misused-promises */
import { NextFunction, Request, RequestHandler, Response } from 'express'
import { QueryResult } from 'pg'

import { pool } from '../database'
import Cabana from '../models/cabana.model'

export const getCabana: RequestHandler = async (_req: Request, res: Response, next: NextFunction) => {
  try {
    const cabanas: Cabana[] = await Cabana.findAll({ attributes: ['id', 'name'] })
    return res.sendData(cabanas, cabanas.length)
  } catch (e) {
    return next(e)
  }
}

export const getCabanaById: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const cabana = await Cabana.findByPk(id)
    return res.sendData(cabana, 1)
  } catch (e) {
    return next(e)
  }
}

export const createCabana: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { name } = req.body
    const cabana = await Cabana.create({ name })
    return res.sendData([cabana], 1)
  } catch (e) {
    return next(e)
  }
}

export const updateCabana: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = parseInt(req.params.id)
    const { name } = req.body
    const cabanaUpdated = await Cabana.update({ name }, { where: { id }, returning: true })
    return res.sendData(cabanaUpdated, 1)
  } catch (e) {
    return next(e)
  }
}

export const deleteCabana: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = parseInt(req.params.id)
    const response: QueryResult = await pool.query('delete from cabanas where id = $1', [id])
    console.log(response)
    return res.status(200).json(
      {
        message: 'Cabaña deleted Successfully',
        body:
        {
          cabana:
          {
            id
          }
        }
      }
    )
  } catch (e) {
    console.log(e)
    return next(e)
  }
}
