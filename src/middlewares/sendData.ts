import { NextFunction, Response, Request } from 'express'

declare module 'express-serve-static-core' {
  export interface Response {
    // eslint-disable-next-line @typescript-eslint/method-signature-style
    sendData(data: unknown, total: number): void
  }
}

export function sendDataMiddleware (_request: Request, response: Response, next: NextFunction): void {
  response.sendData = (data: any, total: number): void => {
    const payload = {
      status: 200,
      meta: {
        count: 1,
        total: 0,
        pages: 0
      },
      data: null
    }

    if (Array.isArray(data)) {
      payload.meta.pages = Math.ceil(total / 25)
      payload.meta.count = data.length
      payload.meta.total = total
    }
    payload.data = data

    response.send(payload)
  }
  return next()
}
