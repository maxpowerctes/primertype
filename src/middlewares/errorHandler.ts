import { ErrorRequestHandler } from 'express'

export const erroHandler: ErrorRequestHandler = (err, _req, res, _next) => {
  const errorResponse = {
    name: 'error',
    code: 'error',
    status: 500,
    message: err.message,
    error: {},
    fields: undefined
  }

  res.status(errorResponse.status).json(errorResponse)
}
