import { RequestHandler } from 'express'

import { ValidationChain, validationResult } from 'express-validator'

export const validator = (validations: ValidationChain[]): RequestHandler => async (req, res, next) => {
  await Promise.all(validations.map(async (validation) => await validation.run(req)))
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    next()
  } else {
    res.status(422).json({ errors: errors.array() })
  }
}
