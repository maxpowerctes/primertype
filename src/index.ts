import express from 'express'
import { erroHandler } from './middlewares/errorHandler'
import indexRoutes from './routes/index'

// const PORT = ((process.env.PORT ?? '').length > 0 || 3000)
const PORT = 3000

const app = express()
app.use(express.json())

app.use(indexRoutes)

// Error handler
app.use(erroHandler)

app.get('/ping', (_req, res) => {
  console.log('someone pinged here!!')
  res.send('pong')
})

app.listen(PORT, () => {
  console.log('Server running on port ', PORT)
})
