/* eslint-disable n/no-path-concat */
import { Pool } from 'pg'

import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { Sequelize } from 'sequelize-typescript'
import { DBConnection } from './types'
// import { Cabana } from './models/cabana.model'
dotenv.config()

const config: DBConnection = {
  user: process.env.USERS ?? 'admin',
  host: process.env.HOST ?? 'localhost',
  password: process.env.PASSWORD ?? 'xxxxxx',
  database: process.env.DATABASE ?? 'zzzzz',
  port: process.env.PORT ?? 5432
}

export const pool = new Pool({
  user: config.user,
  host: config.host,
  password: config.password,
  database: config.database,
  port: config.port as unknown as number,
  ssl: {
    rejectUnauthorized: false
  }
})

// reliyiw851@canyona.com

// Soltem99+

console.log(__dirname + './models/**.model.ts')

const sequelize = new Sequelize({
  database: config.database,
  username: config.user,
  password: config.password,
  host: config.host,
  dialect: 'postgres',
  port: config.port as unknown as number,
  // eslint-disable-next-line n/no-path-concat
  // models: [__dirname + '/models/*model.js'],
  // modelPaths: [__dirname + '/models/*.models'],
  // models: ['/home/luciano/Escritorio/primertype/src/models/**.model.ts'],
  models: [__dirname + '/models'],
  modelMatch: (filename, member) => {
    return filename.substring(0, filename.indexOf('.model')) === member
  },

  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false
    }
  }
})

const test = async (sequelize: Sequelize): Promise<void> => {
  try {
    console.log('Connection has been established successfully.')
    // await sequelize.sync({ force: true })
    await sequelize.authenticate()
  } catch (error) {
    console.error('Unable to connect to the database:', error)
  }
}

test(sequelize).then(res => res).catch(err => err)
