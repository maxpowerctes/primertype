"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e;
Object.defineProperty(exports, "__esModule", { value: true });
exports.pool = void 0;
/* eslint-disable n/no-path-concat */
const pg_1 = require("pg");
const dotenv = __importStar(require("dotenv")); // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
const sequelize_typescript_1 = require("sequelize-typescript");
// import { Cabana } from './models/cabana.model'
dotenv.config();
const config = {
    user: (_a = process.env.USERS) !== null && _a !== void 0 ? _a : 'admin',
    host: (_b = process.env.HOST) !== null && _b !== void 0 ? _b : 'localhost',
    password: (_c = process.env.PASSWORD) !== null && _c !== void 0 ? _c : 'xxxxxx',
    database: (_d = process.env.DATABASE) !== null && _d !== void 0 ? _d : 'zzzzz',
    port: (_e = process.env.PORT) !== null && _e !== void 0 ? _e : 5432
};
exports.pool = new pg_1.Pool({
    user: config.user,
    host: config.host,
    password: config.password,
    database: config.database,
    port: config.port,
    ssl: {
        rejectUnauthorized: false
    }
});
// reliyiw851@canyona.com
// Soltem99+
console.log(__dirname);
const sequelize = new sequelize_typescript_1.Sequelize({
    database: config.database,
    username: config.user,
    password: config.password,
    host: config.host,
    dialect: 'postgres',
    port: config.port,
    // eslint-disable-next-line n/no-path-concat
    models: [__dirname + '/models/*model.js'],
    // modelPaths: [__dirname + '/models/*.models'],
    // models: [Cabana],
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: false
        }
    }
});
const test = (sequelize) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield sequelize.authenticate();
        console.log('Connection has been established successfully.');
        yield sequelize.sync({ force: true });
    }
    catch (error) {
        console.error('Unable to connect to the database:', error);
    }
});
test(sequelize).then(res => res).catch(err => err);
