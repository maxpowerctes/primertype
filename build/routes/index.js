"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sendData_1 = require("../middlewares/sendData");
// import animalRouter from './animalesRoute'
const cabanasRoute_1 = __importDefault(require("./cabanasRoute"));
const router = (0, express_1.default)();
console.log(process.env.PORT);
router.use(sendData_1.sendDataMiddleware);
router.use('/cabana', cabanasRoute_1.default);
// router.use('/animal', animalRouter)
exports.default = router;
