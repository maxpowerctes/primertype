"use strict";
// import { NextFunction, Request, RequestHandler, Response } from 'express'
// import { QueryResult } from 'pg'
// import { pool } from '../database'
// export const getAnimales: RequestHandler = async (_req: Request, res: Response, next: NextFunction) => {
//   try {
//     const response: QueryResult = await pool.query('select * from animales')
//     return res.status(200).json(response.rows)
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const getAnimalesById = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const response: QueryResult = await pool.query('select * from animales where id = $1', [id])
//     return res.status(200).json(response.rows)
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const createAnimales: RequestHandler = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const { name, color, breed, idcabanacriadora, sex } = req.body
//     const response: QueryResult = await pool.query('insert into animales (name, color, breed, idcabanacriadora, sex) values ($1,$2,$3,$4,$5)', [name, color, breed, idcabanacriadora, sex])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal created Successfully',
//         body:
//         {
//           Animal:
//           {
//             name,
//             color,
//             breed,
//             idcabanacriadora,
//             sex
//           }
//         }
//       })
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const updateAnimales: RequestHandler = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const { name, color, breed, idcabanacriadora, sex } = req.body
//     const response: QueryResult = await pool.query('update animales set name = $1, color = $2, breed = $3, idcabanacriadora = $4, sex = $5 where id = $6', [name, color, breed, idcabanacriadora, sex, id])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal updated Successfully',
//         body:
//         {
//           Animal:
//           {
//             id,
//             name,
//             color,
//             breed,
//             idcabanacriadora,
//             sex
//           }
//         }
//       }
//     )
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const deleteAnimales: RequestHandler = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const response: QueryResult = await pool.query('delete from animales where id = $1', [id])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal deleted Successfully',
//         body:
//         {
//           Animal:
//           {
//             id
//           }
//         }
//       }
//     )
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
