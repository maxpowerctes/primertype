"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCabana = exports.updateCabana = exports.createCabana = exports.getCabanaById = exports.getCabana = void 0;
const database_1 = require("../database");
const cabana_model_1 = require("../models/cabana.model");
const getCabana = (_req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const cabanas = yield cabana_model_1.Cabana.findAll();
        return res.sendData(cabanas, cabanas.length);
    }
    catch (e) {
        return next(e);
    }
});
exports.getCabana = getCabana;
const getCabanaById = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const response = yield database_1.pool.query('select * from cabanas where id = $1', [id]);
        return res.sendData(response.rows, response.rows.length);
    }
    catch (e) {
        console.log(e);
        return next(e);
    }
});
exports.getCabanaById = getCabanaById;
const createCabana = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name } = req.body;
        const response = yield database_1.pool.query('insert into cabanas (name) values ($1)', [name]);
        console.log(response);
        return res.status(200).json({
            message: 'Cabaña create1d Successfully',
            body: {
                cabana: {
                    name
                }
            }
        });
    }
    catch (e) {
        console.log(e);
        return next(e);
    }
});
exports.createCabana = createCabana;
const updateCabana = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const { name } = req.body;
        const response = yield database_1.pool.query('update cabanas set name = $1 where id = $2', [name, id]);
        console.log(response);
        return res.status(200).json({
            message: 'Cabaña updated Successfully',
            body: {
                cabana: {
                    id,
                    name
                }
            }
        });
    }
    catch (e) {
        console.log(e);
        return next(e);
    }
});
exports.updateCabana = updateCabana;
const deleteCabana = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const response = yield database_1.pool.query('delete from cabanas where id = $1', [id]);
        console.log(response);
        return res.status(200).json({
            message: 'Cabaña deleted Successfully',
            body: {
                cabana: {
                    id
                }
            }
        });
    }
    catch (e) {
        console.log(e);
        return next(e);
    }
});
exports.deleteCabana = deleteCabana;
// // ###############################################   ANIMALES   #############################################################
// export const getAnimales = async (_req: Request, res: Response): Promise<Response> => {
//   try {
//     const response: QueryResult = await pool.query('select * from animales')
//     return res.status(200).json(response.rows)
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const getAnimalesById = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const response: QueryResult = await pool.query('select * from animales where id = $1', [id])
//     return res.status(200).json(response.rows)
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const createAnimales = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const { name, color, breed, idcabanacriadora, sex } = req.body
//     const response: QueryResult = await pool.query('insert into animales (name, color, breed, idcabanacriadora, sex) values ($1,$2,$3,$4,$5)', [name, color, breed, idcabanacriadora, sex])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal created Successfully',
//         body:
//         {
//           Animal:
//           {
//             name,
//             color,
//             breed,
//             idcabanacriadora,
//             sex
//           }
//         }
//       })
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const updateAnimales = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const { name, color, breed, idcabanacriadora, sex } = req.body
//     const response: QueryResult = await pool.query('update animales set name = $1, color = $2, breed = $3, idcabanacriadora = $4, sex = $5 where id = $6', [name, color, breed, idcabanacriadora, sex, id])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal updated Successfully',
//         body:
//         {
//           Animal:
//           {
//             id,
//             name,
//             color,
//             breed,
//             idcabanacriadora,
//             sex
//           }
//         }
//       }
//     )
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
// }
// export const deleteAnimales = async (req: Request, res: Response): Promise<Response> => {
//   try {
//     const id = parseInt(req.params.id)
//     const response: QueryResult = await pool.query('delete from animales where id = $1', [id])
//     console.log(response)
//     return res.status(200).json(
//       {
//         message: 'Animal deleted Successfully',
//         body:
//         {
//           Animal:
//           {
//             id
//           }
//         }
//       }
//     )
//   } catch (e) {
//     console.log(e)
//     return res.status(500).json('Internal Server Error')
//   }
