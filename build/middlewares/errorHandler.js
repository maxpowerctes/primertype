"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.erroHandler = void 0;
const erroHandler = (err, _req, res, _next) => {
    const errorResponse = {
        name: 'error',
        code: 'error',
        status: 500,
        message: err.message,
        error: {},
        fields: undefined
    };
    res.status(errorResponse.status).json(errorResponse);
};
exports.erroHandler = erroHandler;
