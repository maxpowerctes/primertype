"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendDataMiddleware = void 0;
function sendDataMiddleware(_request, response, next) {
    response.sendData = (data, total) => {
        const payload = {
            status: 200,
            meta: {
                count: 1,
                total: 0,
                pages: 0
            },
            data: null
        };
        if (Array.isArray(data)) {
            payload.meta.pages = Math.ceil(total / 25);
            payload.meta.count = data.length;
            payload.meta.total = total;
        }
        payload.data = data;
        response.send(payload);
    };
    return next();
}
exports.sendDataMiddleware = sendDataMiddleware;
