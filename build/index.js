"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const errorHandler_1 = require("./middlewares/errorHandler");
const index_1 = __importDefault(require("./routes/index"));
// const PORT = ((process.env.PORT ?? '').length > 0 || 3000)
const PORT = 3000;
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use(index_1.default);
// Error handler
app.use(errorHandler_1.erroHandler);
app.get('/ping', (_req, res) => {
    console.log('someone pinged here!!');
    res.send('pong');
});
app.listen(PORT, () => {
    console.log('Server running on port ', PORT);
});
